﻿using System;
namespace ModuloBaseSM22.Clases
{
    public class Persona
    {
        public string Nombre { get; set; }

        public double SueldoMensual { get; set; }

        public double SueldoAnual {
            get
            {
                return SueldoMensual * 12;
            }
        }

        // forma larga
        // private string nombre;

        /*public string Nombre
        {
            get
            {
                Console.WriteLine("Se obtuvo el nombre de la persona");
                return nombre;
            }
            set
            {
                Console.WriteLine("Se cambio el nombre de la persona");
                nombre = value;
            }
        }*/
    }
}
